package models

import models.expressions.*

object AstPrinter : ExpressionVisitor<String> {
    fun print(expression: Expression): String? {
        return expression.accept(this)
    }

    override fun visitBinaryExpression(expression: Binary): String {
        return parenthesize(expression.operator.lexeme, expression.left, expression.right)
    }

    override fun visitGroupingExpression(expression: Grouping): String {
        return parenthesize("group", expression.expression)
    }

    override fun visitLiteralExpression(expression: Literal): String {
        if (expression.value == null) return "nil"
        return expression.value.toString()
    }

    override fun visitUnaryExpression(expression: Unary): String {
        return parenthesize(expression.operator.lexeme, expression.right)
    }

    private fun parenthesize(name: String, vararg expression: Expression): String {
        val builder = StringBuilder()
        builder.append("(").append(name)
        for (expr in expression) {
            builder.append(" ")
            builder.append(expr.accept(this))
        }
        builder.append(")")
        return builder.toString()
    }
}
