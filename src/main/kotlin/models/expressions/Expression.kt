package models.expressions

interface Expression {
    fun <R> accept(visitor: ExpressionVisitor<R>?): R?
}
