package models.expressions

interface ExpressionVisitor<R> {
    fun visitBinaryExpression(expression: Binary): R
    fun visitGroupingExpression(expression: Grouping): R
    fun visitLiteralExpression(expression: Literal): R
    fun visitUnaryExpression(expression: Unary): R
}
