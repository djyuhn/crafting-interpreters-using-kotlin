package models.expressions

data class Grouping(val expression: Expression) : Expression {
    override fun <R> accept(visitor: ExpressionVisitor<R>?): R? {
        return visitor?.visitGroupingExpression(this)
    }
}
