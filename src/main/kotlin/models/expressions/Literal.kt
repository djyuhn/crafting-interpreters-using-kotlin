package models.expressions

data class Literal(val value: Any?) : Expression {
    override fun <R> accept(visitor: ExpressionVisitor<R>?): R? {
        return visitor?.visitLiteralExpression(this)
    }
}
