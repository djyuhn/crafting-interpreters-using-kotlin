package models.expressions

import models.Token

data class Unary(val operator: Token, val right: Expression) : Expression {
    override fun <R> accept(visitor: ExpressionVisitor<R>?): R? {
        return visitor?.visitUnaryExpression(this)
    }
}
